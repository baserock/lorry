#!/bin/sh
#
# Creates a hg repository with a single file and a single commit.
#
# Copyright (C) 2013  Codethink Limited
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


set -e

# create the repository
repo="$DATADIR/hg-test-repo"
mkdir "$repo"
cd "$repo"
hg init --quiet

# add the test file
echo "first line" > test.txt
hg add --quiet test.txt

# make a commit
hg commit --user lorry-test-suite --quiet -m "first commit"

# create the .lorry file for the repository
cat <<EOF > $DATADIR/hg-test-repo.lorry
{
  "hg-test-repo": {
    "type": "hg",
    "url": "file://$repo"
  }
}
EOF

# create the working directory
test -d "$DATADIR/work-dir" || mkdir "$DATADIR/work-dir"
